<?php
namespace Apimenti\Seresta\Parser;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Apimenti.Seresta".      *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use Apimenti\Seresta\Annotations as Seresta;

/**
 * Standard controller for the Apimenti.Seresta package 
 *
 * @Flow\Scope("singleton")
 */
class Seredasj {

}

?>