<?php

namespace Apimenti\Seresta\Annotations;

/**
 * Namespace Annotation
 *
 * @Annotation
 * @Target("CLASS")
 */
final class Ns {
	
	/**
	 * Prefix namespace
	 * 
	 * @var string
	 */
	public $prefix;
	
	/**
	 * URI namespace
	 * 
	 * @var string
	 */
	public $uri;
	
	/**
	 * 
	 * @param array $values
	 */
	function __construct(array $values) {
		$this->prefix = $values['prefix'];
		$this->uri = $values['uri'];
	}

}

?>