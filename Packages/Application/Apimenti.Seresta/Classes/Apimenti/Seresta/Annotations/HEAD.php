<?php

namespace Apimenti\Seresta\Annotations;

/**
 * HEAD
 *
 * @Annotation
 * @Target("METHOD")
 */
final class HEAD {
}

?>