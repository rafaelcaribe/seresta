<?php

namespace Apimenti\Seresta\Annotations;

/**
 * Input Annotation
 *
 * @Annotation
 * @Target("METHOD")
 */
final class Output {
	
	/**
	 * Prefix namespace
	 * 
	 * @var string
	 */
	public $mediaType;
	
	/**
	 * URI namespace
	 * 
	 * @var string
	 */
	public $semantic;
	
	/**
	 * 
	 * @param array $values
	 */
	function __construct(array $values) {
		if(isset($values['mediaType'])){
			$this->mediaType = $values['mediaType'];
		}
		
		$this->semantic = $values['semantic'];
	}

}

?>