<?php

namespace Apimenti\Seresta\Annotations;

/**
 * Input Annotation
 *
 * @Annotation
 * @Target("METHOD")
 */
final class Input {
	public $param;
	public $semantic;
	
	/**
	 * 
	 * @param array $values
	 */
	function __construct(array $values) {
		
		$this->param = $values['param'];
		
		$this->semantic = $values['semantic'];
	}
}

?>