<?php

namespace Apimenti\Seresta\Annotations;

/**
 * POST
 *
 * @Annotation
 * @Target("METHOD")
 */
final class POST {
}

?>