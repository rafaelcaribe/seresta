<?php

namespace Apimenti\Seresta\Annotations;

/**
 * DELETE
 *
 * @Annotation
 * @Target("METHOD")
 */
final class DELETE {
}

?>