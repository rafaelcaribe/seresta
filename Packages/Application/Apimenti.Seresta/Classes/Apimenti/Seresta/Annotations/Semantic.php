<?php

namespace Apimenti\Seresta\Annotations;

/**
 * Input Annotation
 *
 * @Annotation
 * @Target({"METHOD", "CLASS", "PROPERTY"})
 */
final class Semantic {
	
	/**
	 * Prefix namespace
	 * 
	 * @var string
	 */
	public $semanticType;
	
	/**
	 * 
	 * @param array $values
	 */
	function __construct(array $values) {
		
		$this->semanticType = $values['value'];
	}

}

?>