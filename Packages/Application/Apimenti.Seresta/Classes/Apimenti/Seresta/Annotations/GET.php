<?php

namespace Apimenti\Seresta\Annotations;

/**
 * GET
 *
 * @Annotation
 * @Target("METHOD")
 */
final class GET {
}

?>