<?php

namespace Apimenti\Seresta\Annotations;

/**
 * PUT
 *
 * @Annotation
 * @Target("METHOD")
 */
final class PUT {
}

?>