<?php
namespace Apimenti\ServiceDemo\Domain\Model;

use TYPO3\Flow\Annotations as Flow;
use Apimenti\Seresta\Annotations as Seresta;

/**
 * Event model
 *
 * @Flow\Entity
 * @Seresta\Ns(prefix="so", uri="http://schema.org")
 * @Seresta\Semantic("so:Event")
 * @author Rafael Ávila <rafael@apimenti.com.br>
 */
class Event {
	/**
	 * @Seresta\Semantic("so:name")
	 * @var string
	 */
	protected $name;
	
	/**
	 * @Seresta\Semantic("so:startDate")
	 * @var \DateTime
	 */
	protected $startDate;
	
	/**
	 * @Seresta\Semantic("so:endDate")
	 * @var \DateTime
	 */
	protected $endDate;
	
	/**
	 * 
	 * @return string
	 * @Seresta\Output(semantic="so:name")
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * 
	 * @param string $name
	 * @Seresta\Input(param="$name", semantic="so:name")
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * 
	 * @return \DateTime
	 * @Seresta\Output(semantic="so:startDate")
	 */
	public function getStartDate() {
		return $this->startDate;
	}

	/**
	 * 
	 * @param \DateTime $startDate
	 * @Seresta\Input(param="$startDate", semantic="so:startDate")
	 */
	public function setStartDate($startDate) {
		$this->startDate = $startDate;
	}

	/**
	 * 
	 * @return \DateTime
	 * @Seresta\Output(semantic="so:endDate")
	 */
	public function getEndDate() {
		return $this->endDate;
	}

	/**
	 * 
	 * @param \DateTime $endDate
	 * @Seresta\Input(param="$endDate", semantic="so:endDate")
	 */
	public function setEndDate($endDate) {
		$this->endDate = $endDate;
	}
}
?>