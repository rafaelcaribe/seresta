<?php
namespace Apimenti\ServiceDemo\Domain\Repository;

use TYPO3\Flow\Annotations as Flow;

/**
 * Event repository
 *
 * @Flow\Scope("singleton")
 * @author Rafael Ávila <rafael@apimenti.com.br>
 */
class EventRepository extends \TYPO3\Flow\Persistence\Repository {	
	
}
?>
