<?php
namespace Apimenti\ServiceDemo\Controller;

use TYPO3\Flow\Annotations as Flow;
use Apimenti\Seresta\Annotations as Seresta;

/**
 * Event controller
 *
 * @Flow\Scope("singleton")
 * @Seresta\REST
 * @Seresta\Ns(prefix="so", uri="http://schema.org")
 */
class EventController extends \TYPO3\Flow\Mvc\Controller\RestController {

	/**
	 * Some human readable description
	 *
	 * @Seresta\GET
	 * 
	 * @param \Apimenti\ServiceDemo\Controller\Apimenti\ServiceDemo\Domain\Model\Event $resource
	 * @Seresta\Input(param="$teste", semantic="so:Event")
	 * 
	 * @Seresta\Output(mediaType="application/json", semantic="so:Event")
	 */
	public function showAction(Apimenti\ServiceDemo\Domain\Model\Event $resource) {
		$this->view->assign('foos', array(
			'bar', 'baz'
		));
	}
	
//	/**
//	 * Some human readable description
//	 *
//	 * @Seresta\GET
//	 * @Seresta\Output(mediaType="application/json", semantic="so:Event")
//	 */
//	public function listAction() {
//		$this->view->assign('foos', array(
//			'bar', 'baz'
//		));
//	}
	
	
	public function describeAction(){
		$res = array();
		$objects = array();
		
		$rest = $this->reflectionService->getClassAnnotation('Apimenti\ServiceDemo\Controller\EventController', 'Apimenti\Seresta\Annotations\Rest');
		
		if($rest){
			$res['meta'] = array();
			
			$namespaces = $this->reflectionService->getClassAnnotations('Apimenti\ServiceDemo\Controller\EventController', 'Apimenti\Seresta\Annotations\Ns');
			foreach($namespaces as $namespace){
				$res['meta']['prefixes'][$namespace->prefix] = $namespace->uri;
			}
			
			$classReflection = new \TYPO3\Flow\Reflection\ClassReflection('Apimenti\ServiceDemo\Controller\EventController');
			$methods = $classReflection->getMethods();
			foreach($methods as $method){
				if($method->isPublic() 
						&& preg_match('/[a-zA-Z]*Action/', $method->getName()) > 0 
						&& preg_match('/initialize[a-zA-Z]*/', $method->getName()) == 0 
						&& $method->getName() != 'describeAction'){
					$link = array();
					
					$output = $this->reflectionService->getMethodAnnotation('Apimenti\ServiceDemo\Controller\EventController', $method->getName(), 'Apimenti\Seresta\Annotations\Output');
//					\TYPO3\Flow\var_dump($output);
					$link['mediaType'] = $output->mediaType;
					
					$inputs = $this->reflectionService->getMethodAnnotations('Apimenti\ServiceDemo\Controller\EventController', $method->getName(), 'Apimenti\Seresta\Annotations\Input');
					
					
					foreach($inputs as $input){
						$link['variables'][str_replace('$', '',$input->param)] = array(
							'model' => $input->semantic
						);
					}
					$uri = $this->uriBuilder
							->reset()
							->setCreateAbsoluteUri(false);
					if(count($inputs)){
						$uri = $uri->uriFor('index', array('resource' => '1'), 'Event', 'Apimenti.ServiceDemo');
						$uri = str_replace('?resource=1', '/{resource}', $uri);
					}else{
						$uri = $uri->uriFor('index', NULL, 'Event', 'Apimenti.ServiceDemo');
					}
					$res['meta']['links'][$uri] = $link;
				}
			}
			
			//\Apimenti\ServiceDemo\Controller\Apimenti\ServiceDemo\Domain\Model\Event
			$model = $this->reflectionService->getClassAnnotation('Apimenti\ServiceDemo\Domain\Model\Event', 'Apimenti\Seresta\Annotations\Semantic');
			
			$res['type'] = 'object';
			$res['model'] = $model->semanticType;
			$res['properties'] = array();
			$classReflection = new \TYPO3\Flow\Reflection\ClassReflection('Apimenti\ServiceDemo\Domain\Model\Event');
			$properties = $classReflection->getProperties();
			foreach($properties as $property){
//				\TYPO3\Flow\var_dump($property->getName());
				$modelMethod = $this->reflectionService->getPropertyAnnotation('Apimenti\ServiceDemo\Domain\Model\Event', $property->getName(),'Apimenti\Seresta\Annotations\Semantic');
				
//				\TYPO3\Flow\var_dump($modelMethod);
				
				if($modelMethod != null){
					$prop = array(
						'type' => 'string',
						'model' => $modelMethod->semanticType
					);

					$res['properties'][$property->getName()] = $prop;
				}
			}
		}
		
		
		\TYPO3\Flow\var_dump($res);
		die();
		
		
//		$res = $this->reflectionService->getMethodTagsValues('Apimenti\ServiceDemo\Controller\EventController', 'indexAction');
//		
//		\TYPO3\Flow\var_dump($res);
//		
//		$res = $this->reflectionService->getMethodParameters('Apimenti\ServiceDemo\Controller\StandardController', 'indexAction');
//		
//		\TYPO3\Flow\var_dump($res);
	}

}

?>